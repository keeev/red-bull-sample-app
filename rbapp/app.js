/* bascic express server */
var api = require('./api/api'); // get the gallery API
var express = require('express');
var path = require('path');
var morgan = require('morgan');
var app = express();

app.get('/', function(req,res) {
  res.render('layout-template', {title: 'Red Bull', message: 'Hello there!'});
});

var server = app.listen(3000, function() {
  var host = server.address().address;
  var port = server.address().port;

  console.log('App listening at http://%s:%s', host, port);
});

/* add jade as templating engine */

/*app.set('views', './views');*/
app.set('views', './src/gallery');
app.set('view engine', 'jade');

/* serving files/assets via express */
app.use(express.static(path.join(__dirname, 'public')));
app.use('public/scripts', express.static(path.join(__dirname, 'public/scripts')));
app.use('public/styles', express.static(path.join(__dirname, 'public/styles')));

//make bower components path available
app.use('/bower_components',  express.static(__dirname + '/bower_components'));

// nur ein versuch um babel zur verfügung zu stellen
app.use('/node_modules',  express.static(__dirname + '/node_modules'));

/* use our gallery api */
app.use(api);
