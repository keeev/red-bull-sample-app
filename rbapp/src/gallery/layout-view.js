import {LayoutView} from 'backbone.marionette';
import template from './layout-template.jade';


export default LayoutView.extend({
  template: template,
  className: 'container',
  tagName: 'img',
  regions: {
    gallery: '.gallery__item',
  },

  attributes() {
    return {
      src : `list-group-item ${(this.model.get('url'))}`,
    };
  }
});
