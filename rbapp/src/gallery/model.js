import {Model} from 'backbone';

export default Model.extend({
  urlRoot: '/api/gallery',
  isActive() {
    return this.collection.active === this;
  }
});
