import {ItemView} from 'backbone.marionette';
import template from './layout-template.jade';

export default ItemView.extend({
  template: template,
  tagName: 'img',

  attributes() {
    return {
      src  : `{this.model.get('url')}`
    };
  }
});
