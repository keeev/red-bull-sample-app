import {ItemView} from 'backbone.marionette';
import {history} from 'backbone';
import template from './template.jade';


export default ItemView.extend({
  template: template,
  className: 'gallery gallery--show container',

  initialize(options = {}) {
    this.model = options.model;
  },

  templateHelpers() {
    return {
      errors: this.model.validationError
    };
  },

  modelEvents: {
    all: 'render'
  }
});
