var Backbone = require('backbone');
var gallery = require('./gallery');

var collection = new Backbone.Collection(gallery);

module.exports = function (api) {
  api.route('api/gallery')
  .get(function(req,res) {
    res.json(collection);
    });
  }
