module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        src: 'src/<%= pkg.name %>.js',
        dest: 'build/<%= pkg.name %>.min.js'
      }
    },
    concat: {
        options: {
          separator: ';',
        },
        dist: {
          src: ['**/*.js'],
          dest: 'public/scripts/rbapp.js',
        },
      },
      watch: {
          files:  [ 'src/gallery/gallery.styl' ],
          tasks:  [ 'style' ],
          options: {
            spawn: false // for grunt-contrib-watch v0.5.0+, "nospawn: true" for lower versions. Without this option specified express won't be reloaded
          }
      },
      express: {
          options: {
            // Override defaults here
          },
          dev: {
            options: {
              script: 'app.js',
              port: 3000,
              debug: false
            }
          }
        },
      watch: {
        express: {
          files:  [ '**/*.js' ],
          tasks:  [ 'express:dev' ],
          options: {
            spawn: false // for grunt-contrib-watch v0.5.0+, "nospawn: true" for lower versions. Without this option specified express won't be reloaded
          }
        }
      },
    "babel": {
      options: {
        sourceMap: true
      },
      dist: {
        files: {
          "public/scripts/bundle.js": ["src/*/*.js", "**/*.js"]
        }
      }
    },
    stylus: {
      options: {
        use: [
          require('jeet'), // use jeet plugin at compile time
          require('rupture') // use rupture plugin at compile time
        ],
        import: [      //  @import 'foo', 'bar/moo', etc. into every .styl file
          'jeet',       //  that is compiled. These might be findable based on values you gave
        ]
      },
      compile: {
        files: {
          'public/styles/styles.css': 'src/gallery/gallery.styl', // 1:1 compile
        }
      }
    },
    concat_css: {
        options: {},
        all: {
          // src: ["**/*.css"],
         src: ["src/*/*.css"],
         dest: "public/styles/styles.css"
        },
      /*files: {
        'public/styles/styles.css' : 'src/gallery/gallery.css',
      },*/
    },
    browserify: {
      dist: {
        options: {
          transform: [["babelify", { "stage": 0 }]]
        },
        files: {
          "public/scripts/bundle.js": "src/main.js"
        }
      }
    }

  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-express-server');
  grunt.loadNpmTasks('grunt-contrib-stylus');
  grunt.loadNpmTasks('grunt-babel');
  grunt.loadNpmTasks('grunt-browserify');
  grunt.loadNpmTasks('grunt-concat-css');



  // Default task(s).
  grunt.registerTask('default', ['concat','babel']);
  grunt.registerTask('server', ['express:dev','watch']);
  grunt.registerTask('style', ['stylus']);
  grunt.registerTask('watchfiles', ['watch']);
  grunt.registerTask('browser', ['browserify']);
  grunt.registerTask('meow', ['babel']);

};
