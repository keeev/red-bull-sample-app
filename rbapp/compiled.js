'use strict';

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

require('./plugins');

var _backbone = require('backbone');

var _backbone2 = _interopRequireDefault(_backbone);

var _jquery = require('jquery');

var _jquery2 = _interopRequireDefault(_jquery);

var _applicationApplication = require('./application/application');

var _applicationApplication2 = _interopRequireDefault(_applicationApplication);

var app = new _applicationApplication2['default']();

_backbone2['default'].history.start();
