// Configure module loader
System.config({
  baseURL: '/rbapp/',
  transpiler: 'babel',
  babelOptions: {

  },

    // Set paths for third-party libraries as modules
    paths: {
      'backbone': 'bower_components/backbone/backbone.js',
      'marionette': 'bower_components/marionette/lib/backbone.marionette.js',
      'jquery': 'bower_components/jquery/dist/jquery.js'
    }
  });
