#Current Branch

**Flat**

_______


# How to run this thing

Grundsätzlich gilt hier wie bei euch wahrscheinlich auch das typische wenn ein neues Projekt aufgesetzt wird.

`npm install`

`bower install` (wird vielleicht zu einem späteren Zeitpunkt noch relevant, derzeit hab ich aber marionette und co. in der packages.js)

## Express Server starten

Läuft via Grunt. Derzeit hab ich im default-Task nur drin, dass das JS concatinated wird, gestartet wird der Express Server samt Watch-Task mit  `grunt server`


### Warum Concat?
Ich lasse concat deswegen drüberlaufen um im jew. Jade-File dann überhaupt eine App einbinden zu können.
Weiß jedoch noch nicht, ob das hier überhaupt der richtige Weg ist bzw. ob dann die `main.js` die ganze App sozusagen ausgibt und ich sie so verwenden kann in der View?!

## API

Hab mir gedacht, ich verwende wie in der marionette-wires App auch eine API die eine URL der Bilder bereitstellt da ich annehme, dass die ja in Zukunft auch von dem zentralen Red Bull Content Server geliefert werden.


## CSS Compile

per Grunt-Task:

`grunt style` (TIL: Grunt mag's nicht, wenn tasks gleich heißen wie das Plugin selbst -> [check](http://stackoverflow.com/a/22409382/1554678))

derzeit wird wie auch im Express-Server definiert `src/gallery/layout-template.jade` als Index genommen wo's auch eingebunden wird das Stylesheet.